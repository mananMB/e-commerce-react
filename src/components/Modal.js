import React from "react";

const Modal = (props) => {
  return (
    <div className="modal">
      <div className="modal-content">
        <div className="modal-header">
          <h4 className="modal-title">{props.title}</h4>
        </div>
        <div className="modal-body">{props.text}</div>
        <div className="modal-footer">
          {props.yesButton && (
            <button className="close-modal" onClick={props.yesButton}>
              Yes
            </button>
          )}
          {props.closeModalOk && (
            <button
              className="close-modal"
              onClick={() => {
                props.closeModalOk();
                if (props.scrollRef) {
                  props.scrollRef.current.scrollIntoView({
                    behavior: "smooth",
                    block: "start",
                    inline: "end",
                  });
                }
              }}
            >
              Ok
            </button>
          )}
          {props.closeModalNo && (
            <button className="close-modal" onClick={props.closeModalNo}>
              No
            </button>
          )}
        </div>
      </div>
    </div>
  );
};

export default Modal;
