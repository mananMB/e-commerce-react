import React from "react";

class Footer extends React.Component {
  render() {
    return (
      <footer>
        <div className="links">
          <div className="link">About Us</div>
          <div className="link">Contact Us</div>
        </div>
        <div className="attribution">Coded by Manan Singh</div>
      </footer>
    );
  }
}

export default Footer;
