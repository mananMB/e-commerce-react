import React, { Component } from "react";
import Modal from "./Modal";
import validator from "validator";
import { toast } from "react-toastify";

class EditProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
    };
  }

  closeModal = () => {
    this.setState(() => {
      return { showModal: false };
    });
  };

  showModal = () => {
    this.setState(() => {
      return { showModal: true };
    });
  };

  validateTitle = () => {
    return validator.isLength(this.props.productBeingEdited.title.trim(), {
      min: 1,
      max: 100,
    });
  };

  validateImageLink = () => {
    return validator.isURL(this.props.productBeingEdited.image);
  };

  validateCost = () => {
    return validator.isDecimal(this.props.productBeingEdited.price.toString(), {
      force_decimal: true,
      decimal_digits: "2",
      locale: "en-US",
    });
  };

  validateDescription = () => {
    return validator.isLength(
      this.props.productBeingEdited.description.trim(),
      {
        min: 50,
        max: 1000,
      }
    );
  };

  validateCategory = () => {
    return validator.isLength(this.props.productBeingEdited.category.trim(), {
      min: 1,
      max: 50,
    });
  };

  allFieldsValid = () => {
    return (
      this.validateImageLink() &&
      this.validateDescription() &&
      this.validateCost() &&
      this.validateTitle() &&
      this.validateCategory()
    );
  };

  updateData = (event) => {
    event.preventDefault();
    if (this.allFieldsValid()) {
      this.props.updateProduct();
      this.props.closeEditPanel();
      toast("Product details have been successfully updated.");
    }
  };

  componentDidMount() {
    this.props.editProductsRef.current.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "end",
    });
  }

  render() {
    return (
      <div
        ref={this.props.editProductsRef}
        className="edit-product flex column"
      >
        {this.state.showModal && (
          <Modal
            title="Are you sure?"
            text="If you close the editing panel your unsaved changes will be lost."
            yesButton={this.props.closeEditPanel}
            closeModalNo={this.closeModal}
          />
        )}
        <div style={{ display: "flex", marginBottom: "2rem" }}>
          <h1 style={{ marginRight: "auto" }}>Update Product</h1>
          <button className="close-edit-panel-button" onClick={this.showModal}>
            <i className="fa-solid fa-xmark"></i>
          </button>
        </div>
        {this.props.productBeingEdited && (
          <form className="update-product-form flex column">
            <label>Title</label>
            <input
              key="product-title"
              type="text"
              value={this.props.productBeingEdited.title}
              onChange={(event) => {
                this.props.handleProductBeingEditedChange("title", event);
              }}
            />
            <p
              className="hint-text"
              id="hint-text-title"
              style={{ color: this.validateTitle() ? "green" : "red" }}
            >
              Product name should not be an empty and maximum of 100 characters
            </p>
            <label>Cost</label>
            <input
              key="product-cost"
              id="cost-field"
              type="text"
              value={this.props.productBeingEdited.price}
              onChange={(event) => {
                this.props.handleProductBeingEditedChange("price", event);
              }}
            />
            <p
              className="hint-text"
              id="hint-text-price"
              style={{ color: this.validateCost() ? "green" : "red" }}
            >
              Cost should be a number with 2 decimal points
            </p>
            <label>Description</label>
            <textarea
              key="product-desc"
              value={this.props.productBeingEdited.description}
              onChange={(event) => {
                this.props.handleProductBeingEditedChange("description", event);
              }}
            />
            <p
              className="hint-text"
              id="hint-text-description"
              style={{ color: this.validateDescription() ? "green" : "red" }}
            >
              Product description should not be an empty string and be minimum
              of 50 characters and maximum of 1000 characters
            </p>
            <label>Category</label>
            <input
              key="product-category"
              type="text"
              value={this.props.productBeingEdited.category}
              onChange={(event) => {
                this.props.handleProductBeingEditedChange("category", event);
              }}
            />
            <p
              className="hint-text"
              id="hint-text-category"
              style={{ color: this.validateCategory() ? "green" : "red" }}
            >
              Category should not be an empty string and be maximum of 50
              characters
            </p>
            <label>Image Link</label>
            <input
              key="product-image"
              type="text"
              value={this.props.productBeingEdited.image}
              onChange={(event) => {
                this.props.handleProductBeingEditedChange("image", event);
              }}
            />
            <p
              className="hint-text"
              id="hint-text-image"
              style={{ color: this.validateImageLink() ? "green" : "red" }}
            >
              Image link should be a valid png or jpg URL
            </p>
            <input
              style={{
                cursor: this.allFieldsValid() ? "pointer" : "default",
                opacity: this.allFieldsValid() ? 1 : 0.5,
              }}
              type="submit"
              onClick={this.updateData}
              value="Update"
            />
          </form>
        )}
      </div>
    );
  }
}

export default EditProduct;
