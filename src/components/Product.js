import React from "react";
import { fakestore } from "../axios-config";
import Loader from "./Loader";
import AddToCartButton from "./AddToCartButton";
import CartQuantityManipulationButton from "./CartQuantityManipulationButton";

class Product extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      product: null,
    };
  }

  componentDidMount() {
    fakestore.get(`/products/${this.props.id}`).then((response) => {
      this.setState({
        product: response.data,
      });
    });
  }

  render() {
    const isProductInCart = () => {
      return Object.values(this.props.cart).find(
        (product) => product.id === this.state.product.id
      );
    };

    return this.state.product ? (
      <>
        <div id="product">
          <div className="image">
            {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
            <img src={this.state.product.image} alt="product-image" />
          </div>
          <div className="details">
            <div className="name">{this.state.product.title}</div>
            <div className="rating">{`${this.state.product.rating.rate}(${this.state.product.rating.count}+)`}</div>
            <div className="cost">{this.state.product.price}</div>
            <div className="description">{this.state.product.description}</div>
            {isProductInCart() ? (
              <CartQuantityManipulationButton
                updateCart={this.props.updateCart}
                product={Object.values(this.props.cart).find(
                  (product) => product.id === this.state.product.id
                )}
              />
            ) : (
              <AddToCartButton
                updateCart={this.props.updateCart}
                product={this.state.product}
              />
            )}
          </div>
        </div>
      </>
    ) : (
      <Loader />
    );
  }

  // return <h1>Product</h1>;
}

export default Product;
