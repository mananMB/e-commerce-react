import React from "react";

const AddToCartButton = (props) => {
  return (
    <button
      className="add-to-cart-button"
      style={{ cursor: "pointer" }}
      onClick={() => props.updateCart(props.product)}
    >
      Add to Cart
    </button>
  );
};

export default AddToCartButton;
