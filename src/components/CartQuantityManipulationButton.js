const CartQuantityManipulationButton = (props) => {
  console.log(props.product);
  return (
    <div className="quantity">
      <button
        className="minus-button"
        onClick={() => {
          if (props.product.count > 1) {
            props.updateCart(props.product, "removeOne");
          } else {
            props.updateCart(props.product, "remove");
          }
        }}
      >
        <i className="fa-solid fa-minus"></i>
      </button>
      <div className="quantity-box">{props.product.count}</div>
      <button
        className="plus-button"
        onClick={() => {
          props.updateCart(props.product);
        }}
      >
        <i className="fa-solid fa-plus"></i>
      </button>
    </div>
  );
};

export default CartQuantityManipulationButton;
