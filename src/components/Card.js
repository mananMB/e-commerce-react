import React from "react";
import Loader from "./Loader";
import { Link } from "react-router-dom";
import AddToCartButton from "./AddToCartButton";
import CartQuantityManipulationButton from "./CartQuantityManipulationButton";

class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
    this.handleImageLoad = this.handleImageLoad.bind(this);
  }

  handleImageLoad() {
    this.setState({
      loading: false,
    });
  }

  isProductInCart = () => {
    return Object.values(this.props.cart).find(
      (product) => product.id === this.props.product.id
    );
  };
  render() {
    this.isProductInCart();
    return (
      <div className="card">
        <div className="product-image">
          {this.state.loading && <Loader />}
          {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
          <img
            style={{ display: this.state.loading ? "none" : "block" }}
            src={this.props.product.image}
            alt="product-image"
            onLoad={this.handleImageLoad}
          />
        </div>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <div className="category">{this.props.product.category}</div>
          <div className="rating">
            {this.props.product.rating.rate}
            {`(${this.props.product.rating.count}+)`}
          </div>
        </div>
        <div className="cost">{this.props.product.price}</div>
        <div className="item-name">{this.props.product.title}</div>
        <div className="item-description">
          {" " + this.props.product.description}
        </div>
        <Link
          to={"product/" + this.props.product.id}
          className="view-product-button"
          style={{ cursor: "pointer" }}
          onClick={() => this.props.openEditPanel(this.props.product)}
        >
          View Product
        </Link>
        {this.isProductInCart() ? (
          <CartQuantityManipulationButton
            updateCart={this.props.updateCart}
            product={Object.values(this.props.cart).find(
              (product) => product.id === this.props.product.id
            )}
          />
        ) : (
          <AddToCartButton
            updateCart={this.props.updateCart}
            product={this.props.product}
          />
        )}
        <button
          className="update-product-button"
          style={{ cursor: "pointer" }}
          onClick={() => this.props.openEditPanel(this.props.product)}
        >
          Update Product
        </button>
      </div>
    );
  }
}

export default Card;
