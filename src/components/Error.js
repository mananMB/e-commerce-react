import React from "react";

class Error extends React.Component {
  render() {
    return (
      <h1 id="error-container">
        Error:{this.props.error.status}::{this.props.error.statusText}
      </h1>
    );
  }
}

export default Error;
