import { Link } from "react-router-dom";
import CartQuantityManipulationButton from "./CartQuantityManipulationButton";

const Cart = (props) => {
  const calculateCartTotal = (cartItems) => {
    return Object.values(cartItems).reduce((total, item) => {
      return total + item.totalItemCost;
    }, 0);
  };

  return (
    <div className="cart-container">
      {Object.keys(props.cartItems).length > 0 ? (
        Object.values(props.cartItems).map((product) => {
          return (
            <CartItemTile
              key={product.id}
              product={product}
              updateCart={props.updateCart}
            />
          );
        })
      ) : (
        <>
          <h1 style={{ textAlign: "center" }}>The cart is empty</h1>
          <Link
            to="/"
            style={{
              color: "blue",
              textDecoration: "underline",
              textAlign: "center",
            }}
          >
            See product listings
          </Link>
        </>
      )}
      <div
        className="cart-total"
        style={{
          textAlign: "end",
          fontSize: "3rem",
          borderTop: "1px solid var(--tickle-me-pink)",
          paddingTop: "1rem",
        }}
      >
        ${parseFloat(calculateCartTotal(props.cartItems)).toFixed(2)}
      </div>
    </div>
  );
};

const CartItemTile = (props) => {
  return (
    <div className="cart-item-tile">
      <div className="image">
        {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
        <img src={props.product.image} alt="product-image" />
      </div>
      <div className="title">
        <Link to={`product/${props.product.id}`}>{props.product.title}</Link>
      </div>
      <div className="container">
        <CartQuantityManipulationButton
          updateCart={props.updateCart}
          product={props.product}
        />
        <div className="total">
          ${parseFloat(props.product.totalItemCost).toFixed(2)}
        </div>
        <button
          className="remove-item-button"
          onClick={() => {
            props.updateCart(props.product, "remove");
          }}
        >
          <i className="fa-solid fa-trash"></i>
        </button>
      </div>
    </div>
  );
};

export default Cart;
