import React from "react";
import Card from "./Card";
import Error from "./Error";
import Loader from "./Loader";
import { fakestore } from "../axios-config";
import EditProduct from "./EditProduct";
import Modal from "./Modal";

class Products extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: null,
      error: null,
      editing: null,
      showModal: false,
      productBringEdited: null,
    };
    this.editProductsRef = React.createRef();
  }

  openEditPanel = (product) => {
    if (!this.state.editing) {
      this.setState({ productBeingEdited: product }, () => {
        this.setState(() => {
          return {
            editing: true,
          };
        });
      });
    } else {
      this.showModal();
    }
  };

  closeEditPanel = () => {
    this.setState({
      editing: false,
      productBeingEdited: "",
      showModal: false,
    });
  };

  updateProduct = () => {
    this.setState((state) => {
      const productsCopy = [...state.products];
      const index = productsCopy.findIndex((product) => {
        return product.id === state.productBeingEdited.id;
      });

      productsCopy[index] = state.productBeingEdited;
      return {
        products: productsCopy,
      };
    });
  };

  closeModal = () => {
    this.setState(() => {
      return { showModal: false };
    });
  };

  showModal = () => {
    this.setState(() => {
      return { showModal: true };
    });
  };

  componentDidMount() {
    fakestore
      .get("products")
      .then((response) => {
        // console.log(response);
        return response.data;
      })
      .then((data) => {
        this.setState({
          products: data,
          error: null,
        });
      })
      .catch((error) => {
        this.setState({
          products: null,
          error: {
            status: error.response.status,
            statusText: error.response.statusText,
          },
        });
      });
  }

  handleProductBeingEditedChange = (key, event) => {
    this.setState((state) => {
      return {
        productBeingEdited: {
          ...state.productBeingEdited,
          [key]: event.target.value,
        },
      };
    });
  };

  render() {
    if (this.state.error) {
      return <Error error={this.state.error} />;
    }
    if (this.state.products === null) {
      return <Loader />;
    }
    if (this.state.products && this.state.products.length > 0) {
      return (
        <div className="flex product-component">
          {this.state.showModal && (
            <Modal
              title="Unfinished changes."
              text="Please finish updating previous before updating another one."
              closeModalOk={this.closeModal}
              scrollRef={this.editProductsRef}
            />
          )}
          <div className="product-catalogue">
            {this.state.products.map((product) => {
              return (
                <Card
                  cart={this.props.cart}
                  updateCart={this.props.updateCart}
                  key={product.id}
                  product={product}
                  openEditPanel={this.openEditPanel}
                />
              );
            })}
          </div>
          {this.state.editing && (
            <EditProduct
              editProductsRef={this.editProductsRef}
              productBeingEdited={this.state.productBeingEdited}
              handleProductBeingEditedChange={
                this.handleProductBeingEditedChange
              }
              updateProduct={this.updateProduct}
              closeEditPanel={this.closeEditPanel}
            />
          )}
        </div>
      );
    }
    if (this.state.products && this.state.products.length === 0) {
      return <Error error={{ status: "204", statusText: "No content" }} />;
    }
  }
}

export default Products;
