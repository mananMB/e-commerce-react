import React from "react";
import { Link } from "react-router-dom";

class Header extends React.Component {
  render() {
    return (
      <header>
        <Link className="logo" to="/">
          <img
            src="https://via.placeholder.com/150x50?text=Website+Logo"
            alt="logo"
          />
        </Link>
        <nav>
          <ul>
            <li>
              <Link to="/" id="home-button">
                Home<i className="fa-solid fa-home"></i>
              </Link>
            </li>
            <li>
              <Link to="/cart" id="cart-button">
                Cart<i className="fa-solid fa-cart-shopping"></i>
              </Link>
            </li>
          </ul>
        </nav>
      </header>
    );
  }
}

export default Header;
