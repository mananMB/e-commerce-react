import React from "react";
import Header from "./components/Header";
import Products from "./components/Products";
import Footer from "./components/Footer";
import { Route, Switch } from "react-router-dom";
import Product from "./components/Product";
import Cart from "./components/Cart";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cart:
        (localStorage.cartItems && JSON.parse(localStorage.cartItems)) || {},
    };
  }

  syncCartToLocalStorage = () => {
    localStorage.cartItems = JSON.stringify(this.state.cart);
  };

  updateCart = (product, action) => {
    const { id, title, image, price } = product;
    const cartItem = { id, title, image, price };

    switch (action) {
      case "remove":
        this.setState(
          (state) => {
            const cartCopy = state.cart;
            delete cartCopy[product.id];
            console.log(Object.keys(cartCopy).length);
            return {
              cart: { ...cartCopy },
            };
          },
          () => {
            toast(`${product.title} removed from cart.`);
            this.syncCartToLocalStorage();
          }
        );
        break;
      case "removeOne":
        this.setState(
          (state) => {
            if (state.cart[product.id].count > 1) {
              return {
                cart: {
                  ...state.cart,
                  [product.id]: {
                    ...cartItem,
                    count: state.cart[product.id].count - 1,
                    totalItemCost:
                      state.cart[product.id].totalItemCost -
                      parseFloat(product.price),
                  },
                },
              };
            }
            this.updateCart(product, "remove");
          },
          () => {
            toast(`${product.title} quantity lowered by one.`);
            this.syncCartToLocalStorage();
          }
        );
        break;
      case "clearCart":
        this.setState(
          () => {
            return { cart: {} };
          },
          () => {
            toast(`Cart cleared.`);
            this.syncCartToLocalStorage();
          }
        );
        break;
      //default action is add to cart
      default:
        this.setState(
          (state) => {
            if (!state.cart[product.id]) {
              return {
                cart: {
                  ...state.cart,
                  [product.id]: {
                    ...cartItem,
                    count: 1,
                    totalItemCost: parseFloat(product.price),
                  },
                },
              };
            }
            return {
              cart: {
                ...state.cart,
                [product.id]: {
                  ...cartItem,
                  count: state.cart[product.id].count + 1,
                  totalItemCost:
                    state.cart[product.id].totalItemCost +
                    parseFloat(product.price),
                },
              },
            };
          },
          () => {
            toast(`${product.title} added to cart.`);
            this.syncCartToLocalStorage();
          }
        );
    }
  };

  render() {
    return (
      <>
        <Header />
        <div className="min-height-80vh">
          <Switch>
            <Route
              exact
              path="/"
              render={() => (
                <Products cart={this.state.cart} updateCart={this.updateCart} />
              )}
            />
            <Route
              path="/product/:id"
              render={(props) => (
                <Product
                  cart={this.state.cart}
                  updateCart={this.updateCart}
                  id={props.match.params.id}
                />
              )}
            />
            <Route
              path="/cart"
              render={() => (
                <Cart
                  cartItems={this.state.cart}
                  updateCart={this.updateCart}
                />
              )}
            />
          </Switch>
          {/*<Products />*/}
        </div>
        <Footer />
        <ToastContainer
          position="bottom-right"
          autoClose={3000}
          hideProgressBar={false}
          newestOnTop={true}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          pauseOnHover
          theme="light"
        />
      </>
    );
  }
}

export default App;
