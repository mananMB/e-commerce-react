import axios from "axios";

const fakestore = axios.create({
  baseURL: "https://fakestoreapi.com",
});

export { fakestore };
